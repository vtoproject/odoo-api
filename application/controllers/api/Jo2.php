<?php
	use chriskacerguis\RestServer\RestController;
	defined('BASEPATH') OR exit('No direct script access allowed');

	require APPPATH . 'libraries/RestController.php';
	require APPPATH . 'libraries/Format.php';

	class Jo2 extends RestController {
	var $table = 'scanout';
	
	function __construct()
	{
			parent::__construct();
			$this->load->model('Jo2Model');
	}

public function index_get() {
		$job_id = $this->get('id');

		if($job_id != null)
		{	
				$query = $this->Jo2Model->getdata($this->table,$job_id);
		} else {
			// $query = false;
			$query = $this->Jo2Model->getdata($this->table,null,null);
		}

		if($query) {
				$this->response([
						'status' => true,
						'data' => $query
				], RestController::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
		} else {
				$this->response([
						'status' => false,
						'message' => 'data not found'
				], RestController::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
		
		}
	}

public function index_get_old() {
		$job_id = $this->get('id');

		if($job_id != null)
		{	
			$joborder = $this->JoModel->getjoborder($job_id)->num_rows();
			$jobkpa = $this->JoModel->getjobkpa($job_id)->num_rows();
			$jobbpl = $this->JoModel->getjobbpl($job_id)->num_rows();
			$jobbpk = $this->JoModel->getjobbpk($job_id)->num_rows();

			if($joborder > 0){
				$job = 'joborder'; 
				$query = $this->JoModel->getdata($job,$job_id);
			} elseif($jobkpa > 0){
				$job = 'kpa'; 
				$query = $this->JoModel->getdata($job,$job_id);
			} elseif($jobbpk > 0){
				$job = 'bpk'; 
				$query = $this->JoModel->getdata($job,$job_id);
			} elseif($jobbpl > 0) {
				$job = 'bpl'; 
				$query = $this->JoModel->getdata($job,$job_id);
			} else {
				$query = false;
			}

		} else {
			// $query = false;
			$query = $this->JoModel->getdata($this->table,null,null);
		}

		if($query) {
				$this->response([
						'status' => true,
						'data' => $query
				], RestController::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
		} else {
				$this->response([
						'status' => false,
						'message' => 'data not found'
				], RestController::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
		
		}
	}

}
?>