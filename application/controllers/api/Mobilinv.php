<?php
	use chriskacerguis\RestServer\RestController;
	defined('BASEPATH') OR exit('No direct script access allowed');

	require APPPATH . 'libraries/RestController.php';
	require APPPATH . 'libraries/Format.php';

	class Mobilinv extends RestController {
	
	function __construct()
	{
			parent::__construct();
			$this->load->model('MobilinvModel','omodel');
	}

	public function index_get() {
		$no_polisi = $this->get('no_polisi');
		$bulan = $this->get('bulan');
		$tahun = $this->get('tahun');

		$getscanout = $this->omodel->getdata($no_polisi,$bulan,$tahun)->result_array();

		foreach ($getscanout as $result) {
			$query2 = array();
			$id_joborder = $result['id_joborder'];

			$ap = $this->omodel->getdata_ap($id_joborder,$bulan,$tahun)->result_array();
			$pj = $this->omodel->getdata_pj($id_joborder,$bulan,$tahun)->result_array();
			
			$sum_inv_ap=0;
			$sum_inv_pj=0;

			if($ap != null){
				foreach($ap as $d){
					$sum_inv_ap = $sum_inv_ap+$d['invoice'];
					$query2[] = array(
						"id_item" => $d['id_item'],
						"nama_item" => $d['nama_item'],
						// "keterangan" => $d['keterangan'],
						"invoice" => $d['invoice'],
						// "cabang" => $d['cabang'],
					);
				}						
			} else {
				$sum_inv_ap = 0;
			}		

			if($pj != null){
				foreach($pj as $d){
					$sum_inv_pj = $sum_inv_pj+$d['invoice'];
					$query2[] = array(
						"id_item" => $d['id_item'],
						"nama_item" => $d['nama_item'],
						// "keterangan" => $d['keterangan'],
						"invoice" => $d['invoice'],
						// "cabang" => $d['cabang'],
						''
					);
				}						
			} else {
				$sum_inv_pj = 0;
			}			

			$query[] = array(
					'id_joborder' => $result['id_joborder'],
					'no_polisi' => $result['no_polisi'],
					'no_job' => $result['job_id'],
					'tipe' => $result['tipe'],
					'size' => $result['size'],
					'customer' => $result['customer'],
					'dari' => $result['pick_up'],
					'tujuan' => $result['tujuan'],
					'jum_container' => $result['jum_container'],
					'curr' => $result['currency'],
					'jum_invoice' => $result['invoice'],
					'job_detail' => $query2,
					'inv_sum' => $sum_inv_pj+$sum_inv_ap
				);
		}

		if($query) {
				$this->response([
						'status' => true,
						'data' => $query
				], RestController::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
		} else {
				$this->response([
						'status' => false,
						'message' => 'No Polisi atau bulan atau tahun tidak ditemukan'
				], RestController::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
		
		}
	}

	public function index_delete() {
		$id = $this->delete('id');
		if($id === null) {
				$this->response([
						'status' => false,
						'message' => 'provide an id'
				], RestController::HTTP_BAD_REQUEST); 
		} else {
				if($this->OModel->deletedata($this->table,$id) > 0) {
						$this->response([
								'status' => true,
								'id_log' => $id,
								'message' => 'deleted success'
						], RestController::HTTP_NO_CONTENT);
				} else {
						$this->response([
								'status' => false,
								'message' => 'id not found'
						], RestController::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
				}
		}
	}
}
?>