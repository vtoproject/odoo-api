<?php
	use chriskacerguis\RestServer\RestController;
	defined('BASEPATH') OR exit('No direct script access allowed');

	require APPPATH . 'libraries/RestController.php';
	require APPPATH . 'libraries/Format.php';

	class Permintaan extends RestController {
	var $table = 'Permintaan';
	
	function __construct()
	{
			parent::__construct();
			$this->load->model('OModel');
			// $this->methods['index_get']['limit'] = 2;
	}
	public function index_get() {
		$id = $this->get('id');
		if($id === null) {
			$query = $this->OModel->getdata($this->table);
		} else {
			$query = $this->OModel->getdata($this->table,$id);
		}

		if($query) {
				$this->response([
						'status' => true,
						'data' => $query
				], RestController::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
		} else {
				$this->response([
						'status' => false,
						'message' => 'data not found'
				], RestController::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
		
		}
	}

	public function index_delete() {
		$id = $this->delete('id');
		if($id === null) {
				$this->response([
						'status' => false,
						'message' => 'provide an id'
				], RestController::HTTP_BAD_REQUEST); 
		} else {
				if($this->OModel->deletedata($this->table,$id) > 0) {
						$this->response([
								'status' => true,
								'id_log' => $id,
								'message' => 'deleted success'
						], RestController::HTTP_NO_CONTENT);
				} else {
						$this->response([
								'status' => false,
								'message' => 'id not found'
						], RestController::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
				}
		}
	}

	public function index_post() {
		$data = [
				'acc' => $this->post('acc'),
				'id_joborder' => $this->post('id_joborder')
		];

		if ($this->OModel->createdata($this->table,$data) > 0) {
				$this->response([
						'status' => true,
						'message' => 'new data has been created'
				], RestController::HTTP_CREATED);
		} else {
				$this->response([
						'status' => false,
						'message' => 'failed create data'
				], RestController::HTTP_BAD_REQUEST);
		}
}

public function index_put() {
		$id = $this->put('id');
		$data = [
			'acc' => $this->get('acc'),
			'id_joborder' => $this->get('id_joborder'),
		];

		if ($this->OModel->updatedata($this->table,$data,$id) > 0) {
				$this->response([
						'status' => true,
						'message' => 'update log has been updated'
				], RestController::HTTP_NO_CONTENT);
		} else {
				$this->response([
						'status' => false,
						'message' => 'failed to update data'
				], RestController::HTTP_BAD_REQUEST);
		}
}	

}
?>