<?php
	use chriskacerguis\RestServer\RestController;
	defined('BASEPATH') OR exit('No direct script access allowed');

	require APPPATH . 'libraries/RestController.php';
	require APPPATH . 'libraries/Format.php';

	class Dn extends RestController {

	function __construct(){
			parent::__construct();
			$this->load->model('DebitnoteModel');
			$this->load->model('InvoiceSchModel');
			$this->methods['index_get']['limit'] = 10;
	}

	public function index_get() {
		$keyword = $this->get('id');
		$limits = $this->get('limits');

		if($limits != null && $keyword === null) 
			{
				$qinvno = $this->DebitnoteModel->getinv($limits)->result();
					// print_r($qinvno);exit();

				foreach($qinvno as $q)
				{
					$result = $this->DebitnoteModel->getdatainv($q->no_invoice_orig)->row();
					// print_r($result);exit();

					$query2 = array();

					$key = $result->id_invoice;
					$qdndetail = $this->DebitnoteModel->getitem($key)->result();

					foreach($qdndetail as $d)
					{
						$query2[] = array(
							"id_item" => $d->id_item,
							"nama_item" => $d->nama_item,
							"keterangan" => $d->keterangan,
							"invoice" => $d->invoice,
						);
					}

					$query[] = array(
						'tanggal' => $result->tanggal,
						'id_invoice' => $result->id_invoice,
						'id_joborder' => $result->id_joborder,
						'cabang' => $result->cabang,
						'materai' => $result->materai,
						'no_invoice' => $result->no_invoice,
						'no_invoice_orig' => $result->no_invoice_orig,
						'no_aju' => $result->no_aju,
						'master_bl' => $result->master_bl,
						'customer' => $result->customer,
						'address' => $result->address,
						'desc_of_good' => $result->desc_of_good,
						'nm_feedervessel' => $result->nm_feedervessel,
						'shipping_type' => $result->shipping_type,
						'gross' => $result->gross,
						'no_con' => $result->no_con,
						'party' => $result->party,
						'jum_package' => $result->jum_package,
						'tipe_package' => $result->tipe_package,
						'no_feedervessel' => $result->no_feedervessel,
						'port_l' => $result->port_l,
						'port_d' => $result->port_d,
						'invoice_detail' => $query2
					);
				}
			} 
		elseif($keyword != null && $limits === null) 
			{
					$result = $this->DebitnoteModel->getdata_id($keyword)->row();

					$key = $result->id_invoice;
					$qdndetail = $this->DebitnoteModel->getitem($key)->result_array();
					foreach($qdndetail as $d){
						$query2[] = array(
							"id_item" => $d['id_item'],
							"nama_item" => $d['nama_item'],
							"keterangan" => $d['keterangan'],
							"invoice" => $d['invoice'],
						);
					}

					$query[] = array(
						'tanggal' => $result->tanggal,
						'id_invoice' => $result->id_invoice,
						'id_joborder' => $result->id_joborder,
						'cabang' => $result->cabang,
						'materai' => $result->materai,
						'no_invoice' => $result->no_invoice,
						'no_invoice_orig' => $result->no_invoice_orig,
						'no_aju' => $result->no_aju,
						'master_bl' => $result->master_bl,
						'customer' => $result->customer,
						'address' => $result->address,
						'desc_of_good' => $result->desc_of_good,
						'nm_feedervessel' => $result->nm_feedervessel,
						'shipping_type' => $result->shipping_type,
						'gross' => $result->gross,
						'no_con' => $result->no_con,
						'party' => $result->party,
						'jum_package' => $result->jum_package,
						'tipe_package' => $result->tipe_package,
						'no_feedervessel' => $result->no_feedervessel,
						'port_l' => $result->port_l,
						'port_d' => $result->port_d,
						'invoice_detail' => $query2
					);
			} 
		elseif($keyword != null && $limits != null)
			{
				$query = false;
			}
		else 
			{
				$qinvno = $this->DebitnoteModel->getinv($limits)->result();

				foreach($qinvno as $q)
				{

					$result = $this->DebitnoteModel->getdatainv($q->no_invoice_orig)->row();

					$query2 = array();

					$key = $result->id_invoice;
					$qdndetail = $this->DebitnoteModel->getitem($key)->result();

					foreach($qdndetail as $d)
					{
						$query2[] = array(
							"id_item" => $d->id_item,
							"nama_item" => $d->nama_item,
							"keterangan" => $d->keterangan,
							"invoice" => $d->invoice,
						);
					}

					$query[] = array(
						'tanggal' => $result->tanggal,
						'id_invoice' => $result->id_invoice,
						'id_joborder' => $result->id_joborder,
						'cabang' => $result->cabang,
						'materai' => $result->materai,
						'no_invoice' => $result->no_invoice,
						'no_invoice_orig' => $result->no_invoice_orig,
						'no_aju' => $result->no_aju,
						'master_bl' => $result->master_bl,
						'customer' => $result->customer,
						'address' => $result->address,
						'desc_of_good' => $result->desc_of_good,
						'nm_feedervessel' => $result->nm_feedervessel,
						'shipping_type' => $result->shipping_type,
						'gross' => $result->gross,
						'no_con' => $result->no_con,
						'party' => $result->party,
						'jum_package' => $result->jum_package,
						'tipe_package' => $result->tipe_package,
						'no_feedervessel' => $result->no_feedervessel,
						'port_l' => $result->port_l,
						'port_d' => $result->port_d,
						'invoice_detail' => $query2
					);
				}					
			}

		if($query) {
				$this->response([
						'status' => true,
						'data' => $query
				], RestController::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
		} else {
				$this->response([
						'status' => false,
						'message' => 'Wrong parameter or data not found'
				], RestController::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

		}
	}
}
?>
