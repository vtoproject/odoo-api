<?php
	use chriskacerguis\RestServer\RestController;
	defined('BASEPATH') OR exit('No direct script access allowed');

	require APPPATH . 'libraries/RestController.php';
	require APPPATH . 'libraries/Format.php';

	class Account extends RestController {
	var $table = 'account';
	
	function __construct()
	{
			parent::__construct();
			$this->load->model('OModel');
			// $this->methods['index_get']['limit'] = 2;
	}
	public function index_get() {
		$id = $this->get('id');
		if($id === null) {
			$query = $this->OModel->getdata($this->table);
		} else {
			$query = $this->OModel->getdata($this->table,$id);
		}

		if($query) {
				$this->response([
						'status' => true,
						'data' => $query
				], RestController::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
		} else {
				$this->response([
						'status' => false,
						'message' => 'data not found'
				], RestController::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
		
		}
	}

	public function index_delete() {
		$searchid = $this->get_searchid($this->table);

		$id = $this->delete('id');
		if($id === null) {
				$this->response([
						'status' => false,
						'message' => 'provide an id'
				], RestController::HTTP_BAD_REQUEST); 
		} else {
				if($this->OModel->deletedata($this->table,$id) > 0) {
						$this->response([
								'status' => true,
								$searchid => $id,
								'message' => 'deleted success'
						], RestController::HTTP_NO_CONTENT);
				} else {
						$this->response([
								'status' => false,
								'message' => 'id not found'
						], RestController::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
				}
		}
	}

	public function index_post() {
		$data = [
			'acc' => $this->post('acc'),
			'acc1' => $this->post('acc1'),
			'acc2' => $this->post('acc2'),
			'acc_acc' => $this->post('acc_acc'),
			'acc_name' => $this->post('acc_name'),
			'acc_cash_bank_BPP' => $this->post('acc_cash_bank_BPP'),
			'acc_cash_bank_BPL' => $this->post('acc_cash_bank_BPL'),
			'acc_cash_bank_KPA' => $this->post(''),
			'acc_cash_bank_BPK' => $this->post(''),
			'acc_cash_bank_1' => $this->post(''),
			'acc_cash_bank_2' => $this->post(''),
			'acc_cash_bank_3' => $this->post(''),
			'acc_cash_bank_4' => $this->post(''),
			'acc_cash_bank_5' => $this->post(''),
			'acc_cash_bank_6' => $this->post(''),
			'acc_cash_bank_7' => $this->post(''),
			'acc_cash_bank_8' => $this->post(''),
			'acc_cash_bank_9' => $this->post(''),
			'acc_cash_bank_10' => $this->post(''),
			'acc_cash_bank_11' => $this->post(''),
			'acc_cash_bank_12' => $this->post(''),
			'acc_cash_bank_13' => $this->post(''),
			'acc_cash_bank_14' => $this->post(''),
			'acc_cash_bank_15' => $this->post(''),
			'acc_cash_bank_16' => $this->post(''),
			'acc_cash_bank_17' => $this->post(''),
			'acc_cash_bank_18' => $this->post(''),
			'acc_cash_bank_19' => $this->post(''),
			'acc_cash_bank_20' => $this->post(''),
			'acc_cash_bank_21' => $this->post(''),
			'acc_cash_bank_22' => $this->post(''),
			'acc_cash_bank_23' => $this->post(''),
			'acc_cash_bank_24' => $this->post(''),
			'acc_cash_bank_25' => $this->post(''),
			'acc_header' => $this->post(''),
			'acc_level' => $this->post(''),
			'acc_type' => $this->post(''),
			'acc_pj_BPP' => $this->post(''),
			'acc_pj_BPL' => $this->post(''),
			'acc_pj_KPA' => $this->post(''),
			'acc_pj_BPK' => $this->post(''),
			'acc_ap_BPP' => $this->post(''),
			'acc_ap_BPL' => $this->post(''),
			'acc_ap_KPA' => $this->post(''),
			'acc_ap_BPK' => $this->post(''),
			'acc_ar_BPP' => $this->post(''),
			'acc_ar_BPL' => $this->post(''),
			'acc_ar_KPA' => $this->post(''),
			'acc_ar_BPK' => $this->post(''),
			'currency' => $this->post('')
		];

		if ($this->OModel->createdata($this->table,$data) > 0) {
				$this->response([
						'status' => true,
						'message' => 'new data has been created'
				], RestController::HTTP_CREATED);
		} else {
				$this->response([
						'status' => false,
						'message' => 'failed create data'
				], RestController::HTTP_BAD_REQUEST);
		}
}

public function index_put() {
		$id = $this->put('id');
		$data = [
			'acc' => $this->get('acc'),
			'id_joborder' => $this->get('id_joborder'),
		];

		if ($this->OModel->updatedata($this->table,$data,$id) > 0) {
				$this->response([
						'status' => true,
						'message' => 'update log has been updated'
				], RestController::HTTP_NO_CONTENT);
		} else {
				$this->response([
						'status' => false,
						'message' => 'failed to update data'
				], RestController::HTTP_BAD_REQUEST);
		}
	}
	
	function get_searchid($table){
		$id = $this->MyLib->idsearch($this->table);
		return $id;
	}

}
?>