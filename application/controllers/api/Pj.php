<?php


	use chriskacerguis\RestServer\RestController;
	defined('BASEPATH') OR exit('No direct script access allowed');

	require APPPATH . 'libraries/RestController.php';
	require APPPATH . 'libraries/Format.php';

	class Pj extends RestController {
	
	function __construct()
	{
			parent::__construct();
			$this->load->model('PertanggungjawabanModel','omodel');
	}
	public function index_get() {
		ini_set('memory_limit', '-1');
		$keyword = $this->get('id');
		$limits = $this->get('limits');

		if($keyword != null && $limits != null)
		{
			$query = null;
			$result = null;
		} 

		if($keyword == null && $limits == null)
		{
			$result = $this->omodel->getdata($keyword,$limits)->result();			
			foreach ($result as $r) {
				$query = $this->omodel->getdata($r->no_kas,$limits)->row();

				$pj_details = array();

				foreach ($result as $re) {
					$pj_details[] = array(
						'id_pertanggung_jawaban' => $r->id_pertanggung_jawaban,
						'id_joborder' => $r->id_joborder,
						'id_pertanggung_jawaban_detail' => $r->id_pertanggung_jawaban_detail,
						'id_item' => $r->id_item,
						'nama_item' => $r->nama_item,
						'keterangan' => $r->keterangan,
						'currency' => $r->currency,
						'bon_putih' => $r->bon_putih,
						'cetak'	=> $r->cetak,
					);
				}

				$query1[] = array(
					'no_kas' => $query->no_kas,
					'tanggal' => $query->tanggal,
					'id_mitra' => $query->id_mitra,
					'mitra' => $query->mitra,
					'address' => $query->address,
					'keterangan' => $query->keterangan_pj,
					'cabang' => $query->cabang,
					'pj_details' => $pj_details
				);
			}	
		}

		if($keyword == null && $limits != null)
		{
				$res = $this->omodel->getdata($keyword,$limits)->result();

				foreach ($res as $r) {
					$query = $this->omodel->getdata($r->no_kas,null)->row();
					$result = $this->omodel->getdata($r->no_kas,null)->result();		

					$pj_details = array();

					if($result != null){
						foreach ($result as $r) {
							$pj_details[] = array(
								'id_pertanggung_jawaban' => $r->id_pertanggung_jawaban,
								'id_joborder' => $r->id_joborder,
								'id_pertanggung_jawaban_detail' => $r->id_pertanggung_jawaban_detail,
								'id_item' => $r->id_item,
								'nama_item' => $r->nama_item,
								'keterangan' => $r->keterangan,
								'currency' => $r->currency,
								'bon_putih' => $r->bon_putih,
								'cetak'	=> $r->cetak,
							);
						}						
					} else {
							$pj_details[] = array(
								'id_pertanggung_jawaban' => null,
								'id_joborder' => null,
								'id_pertanggung_jawaban_detail' => null,
								'id_item' => null,
								'keterangan' => null,
								'currency' => null,
								'bon_putih' => null,
								'cetak'	=> null,
							);						
					}

					$query1[] = array(
						'no_kas' => $query->no_kas,
						'tanggal' => $query->tanggal,
						'id_mitra' => $query->id_mitra,
						'mitra' => $query->mitra,
						'address' => $query->address,
						'keterangan' => $query->keterangan_pj,
						'cabang' => $query->cabang,
						'pj_details' => $pj_details
					);
			}	
		}		

		if($keyword != null && $limits == null)
		{
			$query = $this->omodel->getdata($keyword,$limits)->row();

			if($query == null){
				$result = false;
			} else {
				$result = $this->omodel->getdata($keyword,$limits)->result();				

				$pj_details = array();

				foreach ($result as $r) {
					$pj_details[] = array(
						'id_pertanggung_jawaban' => $r->id_pertanggung_jawaban,
						'id_joborder' => $r->id_joborder,
						'id_pertanggung_jawaban_detail' => $r->id_pertanggung_jawaban_detail,
						'id_item' => $r->id_item,
						'nama_item' => $r->nama_item,
						'keterangan' => $r->keterangan,
						'currency' => $r->currency,
						'bon_putih' => $r->bon_putih,
						'cetak'	=> $r->cetak,
					);
				}
				$query1[] = array(
					'no_kas' => $query->no_kas,
					'tanggal' => $query->tanggal,
					'id_mitra' => $query->id_mitra,
					'mitra' => $query->mitra,
					'address' => $query->address,
					'keterangan' => $query->keterangan_pj,
					'cabang' => $query->cabang,
					'pj_details' => $pj_details
				);								
			}
		}		

		if($result) {
				$this->response([
						'status' => true,
						'data' => $query1
				], RestController::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
		} else {
				$this->response([
						'status' => false,
						'message' => 'Data not found or wrong parameter'
				], RestController::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
		
		}
	}
}
?>