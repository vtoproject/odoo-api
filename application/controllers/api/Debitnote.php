<?php
	use chriskacerguis\RestServer\RestController;
	defined('BASEPATH') OR exit('No direct script access allowed');

	require APPPATH . 'libraries/RestController.php';
	require APPPATH . 'libraries/Format.php';

	class Debitnote extends RestController {

	function __construct(){
			parent::__construct();
			$this->load->model('DebitnoteModel','modls');
			$this->methods['index_get']['limit'] = 10;
	}

	public function index_get($keyword=null,$limits=null) {
		$keyword = $this->get('id');
		$limits = $this->get('limits');

		if($keyword != null && $limits===null){
			$query = $this->modls->getdata($keyword);
		} elseif($keyword === null && $limits!=null){
			$query = $this->modls->getdatalimits($limits);
		} else {
			$query = $this->modls->getdatanull();
		}

		// if($startdate != null && $enddate != null && $keyword != null){
		// 	$query = $this->modls->getdatabyiddate($keyword,$startdate,$enddate);
		// } elseif ($startdate != null && $enddate != null && $keyword === null){
		// 	$query = $this->modls->getdatabydate($startdate,$enddate);
		// } elseif ($startdate === null && $enddate === null && $keyword != null){
		// 	$query = $this->modls->getdata($keyword);
		// } elseif ($keyword === null && $startdate === null && $enddate == null) {
		// 	$query = $this->modls->getdatanull();
		// } elseif ($keyword != null && $startdate != null && $enddate == null) {
		// 	$query = false;
		// 	// $query = $this->modls->getdataidstartdate($keyword,$startdate);
		// } elseif ($keyword != null && $startdate === null && $enddate != null) {
		// 	$query = false;
		// 	// $query = $this->modls->getdataidenddate($keyword,$enddate);
		// } elseif ($limits != null && $keyword === null && $startdate === null && $enddate === null) {
		// 	// $query = false;
		// 	$query = $this->modls->getdatalimits($limits);
		// } else {
		// 	$query = false;
		// }

		if($query) {
				$this->response([
						'status' => true,
						'data' => $query
				], RestController::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
		} else {
				$this->response([
						'status' => false,
						'message' => 'wrong parameter or data not found'
				], RestController::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
		
		}
	}
}
?>