<?php
use chriskacerguis\RestServer\RestController;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';

class Log extends RestController {
  function __construct()
  {
          parent::__construct();
          $this->load->model('OModel');
          $this->methods['index_get']['limit'] = 2;
  }
  public function index_get() {
  
    $keyword1 = $this->get('id');

    if($keyword1 === null) {                  
      $query = $this->OModel->getdata('logs');
    } else {     
      $query = $this->OModel->getdata('logs',$keyword1);
    }

    if($query) {
      $this->response([
        'status' => true,
        'data' => $query
      ], RestController::HTTP_OK); 
    } else {
      $this->response([
        'status' => false,
        'message' => 'data not found'
      ], RestController::HTTP_NOT_FOUND);       
    }
  }
}
?>