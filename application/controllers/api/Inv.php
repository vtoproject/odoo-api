<?php
	use chriskacerguis\RestServer\RestController;
	defined('BASEPATH') OR exit('No direct script access allowed');

	require APPPATH . 'libraries/RestController.php';
	require APPPATH . 'libraries/Format.php';

	class Inv extends RestController {

	function __construct(){
			parent::__construct();
			$this->load->model('InvoiceSchModel');
			$this->methods['index_get']['limit'] = 10;
	}

	public function index_get() 
	{
		$keyword = $this->get('id');
		$limits = $this->get('limits');

		if($limits != null && $keyword === null) 
		{
			$getinv = $this->InvoiceSchModel->getdatalimits($limits);

			foreach ($getinv as $g) {
				$keyword = $g->no_invoice;
				$query= $this->InvoiceSchModel->getdata($keyword);
				$result = $this->InvoiceSchModel->getdata_id($keyword);
				$query2 = array();

					$qinvdetail = $this->InvoiceSchModel->invoice_detail_id($g->id_invoice);
					$qcontainer = $this->InvoiceSchModel->getdatacontainer($g->id_joborder);

					foreach($qinvdetail as $key => $d)
					{
						$query2[] = array(
							"id_invoice" => $d->id_invoice,
							"id_invoice_detail" => $d->id_invoice_detail,
							"id_item" => $d->id_item,
							"nama_item" => $d->nama_item,
							"keterangan" => $d->keterangan,
							"currency" => $d->currency,
							"invoice" => $d->invoice,
							"ppn" => $d->ppn
						);
					}

					$qcon = '';
						foreach($qcontainer as $qc){
							$qcon = $qc->no_con.','.$qcon;
						}

					$qcontainer = rtrim($qcon,',');

					$query1[] = array(
						'cabang' => $result->cabang,
						'id_joborder' => $result->id_joborder,
						'id_invoice' => $result->id_invoice,
						'tanggal' => $result->tanggal,
						'no_invoice' => $result->no_invoice,
						'no_inv_cus' => $result->no_inv_cus,
						'master_bl' => $result->master_bl,
						'no_aju' => $result->no_aju,
						'customer' => $result->customer,
						'address' => $result->address,
						'desc_of_good' => $result->desc_of_good,
						'nm_feedervessel' => $result->nm_feedervessel,
						'shipping_type' => $result->shipping_type,
						'gross' => $result->gross,
						// 'no_con' => $result->no_con,
						'no_con' => $qcontainer,
						'party' => $result->party,
						'jum_package' => $result->jum_package,
						'tipe_package' => $result->tipe_package,
						'no_feedervessel' => $result->no_feedervessel,
						'port_l' => $result->port_l,
						'port_d' => $result->port_d,
						'materai' => $result->materai,
						'invoice_detail' => $query2
					);				
			}
		}			
		elseif($keyword != null && $limits === null) 
		{
			$query= $this->InvoiceSchModel->getdata($keyword);
			$result = $this->InvoiceSchModel->getdata_id($keyword);
			$query1 = array();

				$qinvoicedetail = $this->InvoiceSchModel->invoice_detail_id($result->id_invoice);
				$querycontainer = $this->InvoiceSchModel->getdatacontainer($result->id_joborder);

				foreach($qinvoicedetail as $key => $d)
				{
					$query2[] = array(
						"id_invoice_detail" => $d->id_invoice_detail,
						"id_item" => $d->id_item,
						"nama_item" => $d->nama_item,
						"keterangan" => $d->keterangan,
						"currency" => $d->currency,
						"invoice" => $d->invoice,
						"ppn" => $d->ppn						
					);
				}

				$qcon = '';
					foreach($querycontainer as $qc){
						$qcon = $qc->no_con.','.$qcon;
					}

				$qcontainer = rtrim($qcon,',');

				$query1[] = array(
					'cabang' => $result->cabang,
					'id_joborder' => $result->id_joborder,
					'id_invoice' => $result->id_invoice,
					'tanggal' => $result->tanggal,
					'no_invoice' => $result->no_invoice,
					'no_inv_cus' => $result->no_inv_cus,
					'master_bl' => $result->master_bl,
					'no_aju' => $result->no_aju,
					'customer' => $result->customer,
					'address' => $result->address,
					'desc_of_good' => $result->desc_of_good,
					'nm_feedervessel' => $result->nm_feedervessel,
					'shipping_type' => $result->shipping_type,
					'gross' => $result->gross,
					// 'no_con' => $result->no_con,
					'no_con' => $qcontainer,
					'party' => $result->party,
					'jum_package' => $result->jum_package,
					'tipe_package' => $result->tipe_package,
					'no_feedervessel' => $result->no_feedervessel,
					'port_l' => $result->port_l,
					'port_d' => $result->port_d,
					'materai' => $result->materai,
					'invoice_detail' => $query2
				);
		} 
		elseif($keyword != null && $limits != null)
		{
			$query1 = false;
		}
		else 
		{
			$getinv = $this->InvoiceSchModel->getdatalimits($limits);

			foreach ($getinv as $g) {
				$keyword = $g->no_invoice;
				$query= $this->InvoiceSchModel->getdata($keyword);
				$result = $this->InvoiceSchModel->getdata_id($keyword);
				$query2 = array();

					$qinvdetail = $this->InvoiceSchModel->invoice_detail_id($g->id_invoice);
					$qcontainer = $this->InvoiceSchModel->getdatacontainer($g->id_joborder);

					foreach($qinvdetail as $key => $d)
					{
						$query2[] = array(
							"id_invoice" => $d->id_invoice,
							"id_invoice_detail" => $d->id_invoice_detail,
							"id_item" => $d->id_item,
							"nama_item" => $d->nama_item,
							"keterangan" => $d->keterangan,
							"currency" => $d->currency,
							"invoice" => $d->invoice,
							"ppn" => $d->ppn
						);
					}

					$qcon = '';
						foreach($qcontainer as $qc){
							$qcon = $qc->no_con.','.$qcon;
						}

					$qcontainer = rtrim($qcon,',');

					$query1[] = array(
						'cabang' => $result->cabang,
						'id_joborder' => $result->id_joborder,
						'id_invoice' => $result->id_invoice,
						'tanggal' => $result->tanggal,
						'no_invoice' => $result->no_invoice,
						'no_inv_cus' => $result->no_inv_cus,
						'master_bl' => $result->master_bl,
						'no_aju' => $result->no_aju,
						'customer' => $result->customer,
						'address' => $result->address,
						'desc_of_good' => $result->desc_of_good,
						'nm_feedervessel' => $result->nm_feedervessel,
						'shipping_type' => $result->shipping_type,
						'gross' => $result->gross,
						// 'no_con' => $result->no_con,
						'no_con' => $qcontainer,
						'party' => $result->party,
						'jum_package' => $result->jum_package,
						'tipe_package' => $result->tipe_package,
						'no_feedervessel' => $result->no_feedervessel,
						'port_l' => $result->port_l,
						'port_d' => $result->port_d,
						'materai' => $result->materai,
						'invoice_detail' => $query2
					);				
			}
		}

		if($query1) {
				$this->response([
						'status' => true,
						'data' => $query1
				], RestController::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
		} else {
				$this->response([
						'status' => false,
						'message' => 'Wrong parameter or data not found'
				], RestController::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
		
		}
	}	

}
?>