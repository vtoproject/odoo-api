<?php
	use chriskacerguis\RestServer\RestController;
	defined('BASEPATH') OR exit('No direct script access allowed');

	require APPPATH . 'libraries/RestController.php';
	require APPPATH . 'libraries/Format.php';

	class Invoices extends RestController {

	function __construct(){
			parent::__construct();
			$this->load->model('InvoiceSchModel');
			$this->methods['index_get']['limit'] = 10;
	}

	public function index_get() {
		$keyword = $this->get('id');
		$limits = $this->get('limits');

		if($limits != null && $keyword === null) {
			$query = $this->InvoiceSchModel->getdatalimits($limits);
		} elseif($keyword != null && $limits === null) {
			$query = $this->InvoiceSchModel->getdata($keyword);
		} else {
			$query = $this->InvoiceSchModel->getdata();
		}

		if($query) {
				$this->response([
						'status' => true,
						'data' => $query
				], RestController::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
		} else {
				$this->response([
						'status' => false,
						'message' => 'data not found'
				], RestController::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
		
		}
	}	

}
?>