<?php
	use chriskacerguis\RestServer\RestController;
	defined('BASEPATH') OR exit('No direct script access allowed');

	require APPPATH . 'libraries/RestController.php';
	require APPPATH . 'libraries/Format.php';

	class Invoice_detail extends RestController {
	var $table = 'invoice_detail';
	
	function __construct()
	{
			parent::__construct();
			$this->load->model('OModel');
			// $this->methods['index_get']['limit'] = 2;
	}
	public function index_get() {
		$id = $this->get('id');
		if($id === null) {
			$query = $this->OModel->getdata($this->table);
		} else {
			$query = $this->OModel->getdata($this->table,$id);
		}

		if($query) {
				$this->response([
						'status' => true,
						'data' => $query
				], RestController::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
		} else {
				$this->response([
						'status' => false,
						'message' => 'data not found'
				], RestController::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
		
		}
	}	

}
?>