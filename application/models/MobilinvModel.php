<?php
class MobilinvModel extends CI_Model
{
	
	public function getdata($no_polisi = null, $bulan = null, $tahun = null)
	{

			$this->db->select("b.id_joborder, a.job_id, a.tgl_job, a.order_id AS no_job,a.job_id AS reff,a.shipping_type AS tipe, a.size, a.pick_up, a.tujuan, a.jum_container, b.no_polisi, c.customer, d.no_invoice, e.currency, e.id_item, e.invoice");		
			$this->db->from('jobkpa a');
			$this->db->join('scanout b','b.id_joborder=a.id','INNER');
			$this->db->join('customer c','c.id_customer=a.id_customer','LEFT');
			$this->db->join('invoice d','d.id_joborder=a.id','LEFT');
			$this->db->join('invoice_detail e','e.id_invoice=d.id_invoice','LEFT');
			if($no_polisi != null && $bulan != null && $tahun != null) {
				$this->db->where('b.no_polisi',$no_polisi);
				$this->db->where('left(a.tgl_job,4)=',$tahun,'true');
				$this->db->where('mid(a.tgl_job,6,2)=',$bulan,'true');
			} elseif($no_polisi != null && $bulan === null && $tahun === null) {
				$this->db->where('b.no_polisi',$no_polisi);
			} else {
				$this->db->limit('5000');
			}

		return $this->db->get();
	}

  public function getdata_ap($id_joborder,$bulan,$tahun)
  {
		$this->db->select('b.id_item,a.cabang,c.nama_item,b.keterangan,b.invoice,b.bon_putih');
		// $this->db->select_sum('b.invoice');
		$this->db->from('ap a');
		$this->db->join('ap_detail b','b.id_ap = a.id_ap');
		$this->db->join('item c','c.id_item = b.id_item');
		$this->db->where('a.id_joborder',$id_joborder);
		$this->db->where('a.cabang =','KPA');
		$this->db->where('left(a.tanggal,4)=',$tahun,'true');
		$this->db->where('mid(a.tanggal,6,2)=',$bulan,'true');
		return $this->db->get();
  }

  public function getdata_pj($id_joborder,$bulan,$tahun)
  {
		$this->db->select('b.id_item,a.cabang,c.nama_item,b.keterangan,b.invoice,b.bon_putih');
		// $this->db->select_sum('b.invoice');
		$this->db->from('pertanggung_jawaban a');
		$this->db->join('pertanggung_jawaban_detail b','b.id_pertanggung_jawaban = a.id_pertanggung_jawaban');
		$this->db->join('item c','c.id_item = b.id_item');
		$this->db->where('a.id_joborder',$id_joborder);
		$this->db->where('a.cabang =','KPA');
		$this->db->where('left(a.tanggal,4)=',$tahun,'true');
		$this->db->where('mid(a.tanggal,6,2)=',$bulan,'true');		
		return $this->db->get();
  } 


}
