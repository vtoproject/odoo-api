<?php
class JobkpaModel extends CI_Model
{
	// public function getdata($job_id)
	// {
	// 	$this->db->select("a.id, a.job_id, a.tgl_job, a.order_id AS no_job,a.job_id AS reff,a.shipping_type AS tipe, a.size, a.pick_up, a.tujuan, a.jum_container, b.no_polisi");		
	// 	$this->db->from('jobkpa a');
	// 	$this->db->join('scanout b','b.id_joborder=a.id','INNER');
	// 	$this->db->where('a.id',$job_id);
	// 	return $this->db->get()->result_array();
	// }
	
	public function getdata($job_id)
	{
			$this->db->select("a.id, b.id_joborder, a.job_id, a.tgl_job, a.order_id AS no_job,a.job_id AS reff,a.shipping_type AS tipe, a.size, a.pick_up as dari, a.tujuan, a.jum_container, b.no_polisi, c.customer, d.no_invoice, e.currency, e.id_item, f.nama_item, e.invoice, d.cabang");		
			$this->db->from('jobkpa a');
			$this->db->join('scanout b','b.id_joborder=a.id','INNER');
			$this->db->join('customer c','c.id_customer=a.id_customer','LEFT');
			$this->db->join('invoice d','d.id_joborder=a.id','LEFT');
			$this->db->join('invoice_detail e','e.id_invoice=d.id_invoice','LEFT');
			$this->db->join('item f','f.id_item=e.id_item','LEFT');
			if($job_id != null) {
				$this->db->where('a.id',$job_id);
				$this->db->where('d.cabang','KPA');

			} else {
				$this->db->limit('5000');
			}
		return $this->db->get()->result_array();
	}

  public function getdata_ap($id_joborder)
  {
		$this->db->select('b.id_item,a.cabang,c.nama_item,b.keterangan,b.invoice,b.bon_putih');
		// $this->db->select_sum('b.invoice');
		$this->db->from('ap a');
		$this->db->join('ap_detail b','b.id_ap = a.id_ap');
		$this->db->join('item c','c.id_item = b.id_item');
		$this->db->where('a.id_joborder',$id_joborder);
		$this->db->where('a.cabang =','KPA');
		return $this->db->get();
  }

  public function getdata_pj($id_joborder)
  {
		$this->db->select('b.id_item,a.cabang,c.nama_item,b.keterangan,b.invoice,b.bon_putih');
		// $this->db->select_sum('b.invoice');
		$this->db->from('pertanggung_jawaban a');
		$this->db->join('pertanggung_jawaban_detail b','b.id_pertanggung_jawaban = a.id_pertanggung_jawaban');
		$this->db->join('item c','c.id_item = b.id_item');
		$this->db->where('a.id_joborder',$id_joborder);
		$this->db->where('a.cabang =','KPA');	
		return $this->db->get();
  } 


}
