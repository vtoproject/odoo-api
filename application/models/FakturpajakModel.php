<?php
class FakturpajakModel extends CI_Model
{
	public function getdata($keyword = null){
		$this->db->select("a.tanggal,CONCAT('PT. BANGUN PUTRA PESAKA') AS company,CONCAT('JL. Tipar Cakung No. 52 RT 001/01 Kel. Sukapura Cilincing Jakarta Utara') AS company_address,CONCAT('01.397.436.5-046.000') AS company_npwp, a.`no_invoice`,e.customer,e.address,e.npwp,a.`no_pajak`,b.currency,b.invoice_kurs,SUM(b.`invoice`) AS dasar_pengenaan_pajak,a.`iduser` AS finance");
		$this->db->from('invoice a');
		$this->db->join('invoice_detail b','b.id_invoice=a.id_invoice','LEFT');
		$this->db->join('item c','c.id_item=b.id_item','LEFT');
		$this->db->join('joborder d','d.id=a.id_joborder','LEFT');
		$this->db->join('customer e','e.id_customer=d.id_customer','LEFT');
		if($keyword===null){
			$this->db->limit('5000');
			$this->db->order_by('id','desc');
			$query = $this->db->get();
			return $query->result_array();
		} else {
			$this->db->where('a.no_invoice',$keyword);
			$query = $this->db->get();
			return $query->result_array();
		}
  }
}