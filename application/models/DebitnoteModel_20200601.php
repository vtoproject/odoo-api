<?php
class DebitnoteModel extends CI_Model
{
	public function getdata_id($keyword){
		$query = $this->db->query('SELECT * from dnone WHERE no_invoice="'.$keyword.'"');
		return $query;
  }

  public function getdata_ap($id_joborder)
  {
		$this->db->select('b.id_item,c.nama_item,b.keterangan,b.invoice,b.bon_putih');
		$this->db->from('ap a');
		$this->db->join('ap_detail b','b.id_ap = a.id_ap');
		$this->db->join('item c','c.id_item = b.id_item');
		$this->db->where('a.id_joborder',$id_joborder);
		$this->db->where('b.cetak',1);
		$this->db->where('b.invoice !=',0);
		return $this->db->get();
  }

  public function getdata_pj($id_joborder)
  {
		$this->db->select('b.id_item,c.nama_item,b.keterangan,b.invoice,b.bon_putih');
		$this->db->from('pertanggung_jawaban a');
		$this->db->join('pertanggung_jawaban_detail b','b.id_pertanggung_jawaban = a.id_pertanggung_jawaban');
		$this->db->join('item c','c.id_item = b.id_item');
		$this->db->where('a.id_joborder',$id_joborder);
		$this->db->where('b.cetak',1);
		$this->db->where('b.invoice !=',0);
		return $this->db->get();
  }  

	public function getdatainv($keyword){
		$this->db->select('*');
		$this->db->from('dnone');
		$this->db->where('no_invoice_orig',$keyword);
		return $this->db->get();
  }   

	public function getitem($key)
	{
		$this->db->select('b.*,c.*');
		$this->db->from('invoice a');
		$this->db->join('invoice_detail b','b.id_invoice = a.id_invoice','LEFT');
		$this->db->join('item c','c.id_item = b.id_item','LEFT');
		$this->db->where('b.id_invoice',$key);
		return $this->db->get();
	}  

	public function getitem_old($key)
	{
		$this->db->select('b.*,c.*');
		$this->db->from('invoice a');
		$this->db->join('invoice_detail b','b.id_invoice = a.id_invoice','LEFT');
		$this->db->join('item c','c.id_item = b.id_item','LEFT');
		$this->db->where('b.id_invoice',$key);
		return $this->db->get();
	}

  function getinv($limits){
  	$this->db->select('*');
  	$this->db->from('dnone');
  	if($limits!=null){
	  	$this->db->limit($limits);  		
  	} else {
	  	$this->db->limit(3000);
  	}
  	$this->db->group_by('id_invoice');
  	$this->db->order_by('id_invoice','DESC');
  	return $this->db->get();
  }

}
