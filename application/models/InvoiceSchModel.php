<?php
class InvoiceSchModel extends CI_Model
{
	public function getdata($keyword = null){
		$this->db->select("a.id_invoice,a.materai,a.tanggal, a.`no_invoice`,e.customer,e.address,e.npwp,a.`no_pajak`,b.currency,b.invoice_kurs,d.no_aju,d.master_bl,d.no_inv_cus,d.desc_of_good,d.nm_feedervessel,d.shipping_type,d.jum_package,d.gross,d.tipe_package,d.no_feedervessel,d.port_l,d.port_d,c.*,b.invoice,d.jum_container as party,f.no_con,f.tipe_cont,b.id_item,c.nama_item,f.id_joborder");
		$this->db->from('invoice a');
		$this->db->join('invoice_detail b','b.id_invoice=a.id_invoice','LEFT');
		$this->db->join('item c','c.id_item=b.id_item','LEFT');
		$this->db->join('joborder d','d.id=a.id_joborder','LEFT');
		$this->db->join('customer e','e.id_customer=d.id_customer','LEFT');
		$this->db->join('container f','f.id_joborder=d.id','LEFT');

		if($keyword===null){
			$this->db->order_by('a.id_invoice','DESC')->limit('10000');
			$query = $this->db->get();
			return $query->result();
		} else {
			$this->db->where('a.no_invoice',$keyword);
			$query = $this->db->get();
			return $query->result();
		}
  }

  public function getinv($limits = null){
  	$this->db->select('id_invoice,tanggal,no_invoice,id_joborder');
  	$this->db->from('invoice');
  	$this->db->order_by('id_invoice','DESC');
	$this->db->limit($limits);
  	return $this->db->get();
  }

  public function getdatalimits($limits = null){
		$this->db->select("a.id_invoice, a.tanggal, a.`no_invoice`,e.customer,e.address,e.npwp,a.`no_pajak`,b.currency,b.invoice_kurs,d.no_aju,d.master_bl,d.no_inv_cus,d.desc_of_good,d.nm_feedervessel,d.shipping_type,d.jum_package,d.gross,d.tipe_package,d.no_feedervessel,d.port_l,d.port_d,c.*,b.invoice,d.jum_container as party,f.no_con,f.tipe_cont,b.id_item,c.nama_item,f.id_joborder");
		$this->db->from('invoice a');
		$this->db->join('invoice_detail b','b.id_invoice=a.id_invoice','LEFT');
		$this->db->join('item c','c.id_item=b.id_item','LEFT');
		$this->db->join('joborder d','d.id=a.id_joborder','LEFT');
		$this->db->join('customer e','e.id_customer=d.id_customer','LEFT');
		$this->db->join('container f','f.id_joborder=d.id','LEFT');
		$this->db->order_by('a.id_invoice','DESC');			
		$this->db->limit($limits);
		$query = $this->db->get();
		return $query->result();
  }

	public function invoice_detail_id($id){
		$this->db->from('invoice_detail');
		$this->db->join('item','item.id_item=invoice_detail.id_item','LEFT');
		$this->db->where('id_invoice',$id);
		$query = $this->db->get();
		return $query->result();
	}

	public function getdata_id($id){
		$this->db->select("a.id_invoice,a.materai,a.tanggal,a.`no_invoice`,e.customer,e.address,e.npwp,a.`no_pajak`,b.currency,b.invoice_kurs,d.no_aju,d.master_bl,d.no_inv_cus,d.desc_of_good,d.nm_feedervessel,d.shipping_type,d.jum_package,d.gross,d.tipe_package,d.no_feedervessel,d.port_l,d.port_d,c.*,b.invoice,d.jum_container as party, f.no_con,f.tipe_cont,b.id_item,c.nama_item,a.id_joborder");
		$this->db->from('invoice a');
		$this->db->join('invoice_detail b','b.id_invoice=a.id_invoice','LEFT');
		$this->db->join('item c','c.id_item=b.id_item','LEFT');
		$this->db->join('joborder d','d.id=a.id_joborder','LEFT');
		$this->db->join('customer e','e.id_customer=d.id_customer','LEFT');
		$this->db->join('container f','f.id_joborder=d.id','LEFT');
		$this->db->order_by('a.id_invoice','DESC');			
		// $this->db->limit('1');
		$this->db->where('a.no_invoice',$id);
		$query = $this->db->get();
		return $query->row();		
	}	

	public function getdatacontainer($id){
		$this->db->select('no_con');
		$this->db->from('container');
		$this->db->where('id_joborder',$id);
		$q = $this->db->get();
		// print_r($q);exit();
		return $q->result();
	}
}