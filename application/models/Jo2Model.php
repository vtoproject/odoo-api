<?php
class Jo2Model extends CI_Model
{
	public function getdata($job, $id){
		if($id === null){
			$this->db->select("a.id,a.job_id,a.no_inv_cus,a.master_bl,a.no_aju,b.customer,a.shipping_type,a.gross,c.no_con,a.party,a.jum_package,a.tipe_package,a.no_feedervessel,a.nm_feedervessel,a.port_l,a.port_d,a.tgl_job");
			$this->db->from('joborder a');
			$this->db->join('customer b','b.id_customer=a.id_customer','LEFT');
			$this->db->join('container c','c.id_joborder=a.id','LEFT');			
			$this->db->order_by('job_id','DESC');
			$this->db->limit('5000');	
			return $this->db->get()->result_array();
		} else {
			$this->db->select("a.id, a.job_id, c.no_con, c.tipe_cont, c.con_size, a.pick_up as dari, a.tujuan, d.no_polisi, a.tgl_job, b.customer, a.jum_container,h.nama_item,f.keterangan,f.invoice");
			$this->db->from('jobkpa a');
			$this->db->join('customer b','b.id_customer=a.id_customer','LEFT');
			$this->db->join('container c','c.id_joborder=a.id','LEFT');	
			$this->db->join('scanout d','a.id=d.id_joborder','LEFT');
			$this->db->join('pertanggung_jawaban e','e.id_joborder=a.id');
			$this->db->join('pertanggung_jawaban_detail f','f.id_pertanggung_jawaban_detail=e.id_pertanggung_jawaban');
			$this->db->join('item h','h.id_item=f.id_item');
			$this->db->where('a.id',$id);
			$this->db->where('h.cabang','KPA');
			return $this->db->get()->result_array();
		}
	}
}
?>