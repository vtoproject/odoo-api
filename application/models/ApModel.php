<?php
class ApModel extends CI_Model
{
	function __construct()
	{
			parent::__construct();
	}

  public function getdata($keyword,$limits){
  	// echo $keyword;exit();
		$this->db->select("a.no_kas,a.tanggal,c.id_mitra,d.mitra,d.address,a.keterangan as keterangan_ap,a.cabang,a.id_ap,a.id_joborder,b.keterangan as keterangan,b.id_ap_detail,b.id_item,c.nama_item,b.currency,b.bon_putih,b.cetak");
		$this->db->from('ap a');
		$this->db->join('ap_detail b','b.id_ap=a.id_ap','LEFT');
		$this->db->join('item c','c.id_item=b.id_item','LEFT');
		$this->db->join('mitra d','d.id_mitra=c.id_mitra','LEFT');

		// $this->db->where('cetak',1);

		$this->db->order_by('a.id_ap','desc');

		if($keyword == null && $limits == null){
			$this->db->group_by('a.no_kas');
			$this->db->limit('5000');
		} elseif($keyword != null && $limits == null) {
			$this->db->where('no_kas',$keyword);
		} else {
			$this->db->group_by('a.no_kas');
			$this->db->limit($limits);
		}		
		return $this->db->get();
  }

}