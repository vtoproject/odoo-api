<?php
class LogModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

	public function getLog($keyword1 = null, $keyword2 = null){
		if($keyword1 === null && $keyword2 === null){
			return $this->db->get('log')->result_array();
		} else {
			$this->db->select('*');
			$this->db->from('log');
			$this->db->where('id_log',$keyword1);
			$this->db->or_like('log',$keyword2);
			return $this->db->get()->result_array();
		} 			
	}

	public function deleteLog($keyword) {
			$this->db->delete('log', ['id_log' => $keyword]);
			return $this->db->affected_rows();
	}

	public function createLog($data) {
			$this->db->insert('log', $data);
			return $this->db->affected_rows();
	} 

	public function updateLog($data, $keyword) {
			$this->db->update('log', $data, ['id_log' => $keyword]);
			return $this->db->affected_rows();
	}		
}
?>