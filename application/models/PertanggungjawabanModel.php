<?php
class PertanggungjawabanModel extends CI_Model
{

	public function getdata($keyword=null,$limits=null){
		$this->db->select("a.no_kas,a.tanggal,c.id_mitra,d.mitra,d.address,a.keterangan as keterangan_pj,a.cabang,a.id_pertanggung_jawaban,a.id_joborder,b.id_pertanggung_jawaban_detail,b.keterangan as keterangan,b.id_item,c.nama_item,b.currency,b.bon_putih,b.cetak");
		$this->db->from('pertanggung_jawaban a');
		$this->db->join('pertanggung_jawaban_detail b','b.id_pertanggung_jawaban=a.id_pertanggung_jawaban','LEFT');
		$this->db->join('item c','c.id_item=b.id_item','LEFT');
		$this->db->join('mitra d','d.id_mitra=c.id_mitra','LEFT');

		// $this->db->where('cetak',1);
		
		$this->db->order_by('a.id_pertanggung_jawaban','DESC');

		if($keyword == null && $limits == null){
			$this->db->group_by('no_kas');
			$this->db->where('YEAR(tanggal)', date('Y'));
			// $this->db->limit('10000');
		} elseif($keyword != null && $limits == null) {
			$this->db->where('a.no_kas',$keyword);
		} else {
			$this->db->group_by('no_kas');
			$this->db->limit($limits);
		}

		return $this->db->get();
  }
}