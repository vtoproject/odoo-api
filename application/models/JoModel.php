<?php
class JoModel extends CI_Model
{
	public function getdata($job, $id){
		if($id === null){
			$this->db->select("a.id,a.job_id,a.no_inv_cus,a.master_bl,a.no_aju,b.customer,a.shipping_type,a.gross,c.no_con,a.party,a.jum_package,a.tipe_package,a.no_feedervessel,a.nm_feedervessel,a.port_l,a.port_d,a.tgl_job");
			$this->db->from('joborder a');
			$this->db->join('customer b','b.id_customer=a.id_customer','LEFT');
			$this->db->join('container c','c.id_joborder=a.id','LEFT');			
			$this->db->order_by('job_id','DESC');
			$this->db->limit('5000');	
			return $this->db->get()->result_array();
		} else {
			switch ($job) {
				case 'joborder':
						$this->db->select("a.id,a.job_id,a.no_inv_cus,a.master_bl,a.no_aju,b.customer,a.shipping_type,a.gross,c.no_con,a.party,a.jum_package,a.tipe_package,a.no_feedervessel,a.nm_feedervessel,a.port_l as dari,a.port_d as tujuan,'null' as no_polisi, c.tipe_cont,c.con_size,a.jum_container,a.tgl_job");
						$this->db->from('joborder a');
					break;
				case 'bpk':
						$this->db->select("a.id,a.job_id,'null' as no_inv_cus,'null' as master_bl,'null' as no_aju,b.customer,'null' as shipping_type,'null' as gross,c.no_con,'null' as party,'null' as jum_package,'null' as tipe_package,'null' as no_feedervessel,'null' as nm_feedervessel,'null' as port_l,'null' as port_d,d.no_polisi, c.tipe_cont,c.con_size, c.pick_up as dari, c.tujuan,'null' as jum_container,a.tgl_job");
						$this->db->from('jobbpk a');
						$this->db->join('mobil d','d.idmobil=a.id_mobil','LEFT');			
					break;
				case 'bpl':
						$this->db->select("a.id,a.job_id,a.no_inv_cus,a.master_bl,'null' as no_aju,b.customer,'null' as shipping_type,a.gross,c.no_con,'null' as party,'null' as jum_package,'null' as tipe_package,a.no_feedervessel,a.nm_feedervessel,'null' as port_l,'null' as port_d,'null' as no_polisi, c.tipe_cont,c.con_size, c.pick_up as dari, c.tujuan,a.jml as jum_container,a.tgl_job");
						$this->db->from('jobbpl a');
					break;
				case 'kpa':
						$this->db->select("a.id,a.job_id,a.no_inv_cus,a.master_bl,'null' as no_aju,b.customer,a.shipping_type,a.gross,c.no_con,'null' as party,a.jum_package,a.tipe_package,a.no_feedervessel,a.nm_feedervessel,'null' as port_l,'null' as port_d,'null' as no_polisi, a.tipe,a.size, a.pick_up as dari, a.tujuan, a.jum_container,a.tgl_job");
						$this->db->from('jobkpa a');
					break;															
			}
			$this->db->join('customer b','b.id_customer=a.id_customer','LEFT');
			$this->db->join('container c','c.id_joborder=a.id','LEFT');	
			$this->db->where('a.id',$id);
			return $this->db->get()->result_array();
		}
	}

	public function getjoborder($id)
	{
		$this->db->select('job_id');
		$this->db->from('joborder');
		$this->db->where('id',$id);
		return $this->db->get();
	}	

	public function getjobkpa($id)
	{
		$this->db->select('*');
		$this->db->from('jobkpa');
		$this->db->where('id',$id);		
		return $this->db->get();
	}

	public function getjobbpl($id)
	{
		$this->db->select('*');
		$this->db->from('jobbpl');
		$this->db->where('id',$id);		
		return $this->db->get();	
	}

	public function getjobbpk($id)
	{
		$this->db->select('*');
		$this->db->from('jobbpk');
		$this->db->where('id',$id);		
		return $this->db->get();
	}

}
?>