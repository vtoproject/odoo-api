<?php
class InvoiceModel extends CI_Model
{
	public function getdata($id = null)
	{
		if($id === null){
			// $this->db->limit('100');
			return $this->db->get('invoice')->result_array();
		} else {
			return $this->db->get_where('invoice',['id_invoice'=>$id])->result_array();
		}
  }

	public function deletedata($id)
	{
		$this->db->delete('invoice',['id_invoice' => $id]);
		return $this->db->affected_rows();
	}

	public function getitem($key)
	{
		$this->db->from('invoice a');
		$this->db->join('invoice_detail b','b.id_invoice=a.id_invoice','LEFT');
		$this->db->join('item c','c.id_item=b.id_item','LEFT');
		if($key != null){
			$this->db->where('a.no_invoice',$key);
		}
		return $this->db->get()->result_array();
	}
}
